package org.letmecode.traceete.util

import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

fun <T> compositeWrapper(source: Observable<T>): Observable<T> {
    return source
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

}

fun <T> compositeWrapper(
    source: Observable<T>,
    lvProgress: SingleLiveEvent<Boolean>
): Observable<T> {
    return source
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe {
            lvProgress.value = true
        }
}

fun consumeErrorWrapper(
    lvState: SingleLiveEvent<Boolean>,
    lvMessage: SingleLiveEvent<String>
): Consumer<Throwable> {
    return Consumer {
        lvState.value = false
        lvMessage.value = it.localizedMessage
    }
}

private fun <T> consumeDataListWrapper(lvData: SingleLiveEvent<List<T>>): Consumer<List<T>> {
    return Consumer {
        lvData.value = it
    }
}

fun <T> consumeDataListWrapper(
    lvData: SingleLiveEvent<List<T>>,
    lvState: SingleLiveEvent<Boolean>
): Consumer<List<T>> {
    return Consumer {
        lvData.value = it
        lvState.value = false
    }
}

fun <T> consumeDataWrapper(
    lvData: SingleLiveEvent<T>,
    lvState: SingleLiveEvent<Boolean>
): Consumer<T> {
    return Consumer {
        lvData.value = it
        lvState.value = false
    }
}

fun <T> consumeDataWrapper(
    lvData: MutableLiveData<T>,
    lvState: SingleLiveEvent<Boolean>
): Consumer<T> {
    return Consumer {
        lvData.value = it
        lvState.value = false
    }
}

fun consumeErrorWrapper(
    lvState: SingleLiveEvent<Boolean>
): Consumer<Throwable> {
    return Consumer {
        lvState.value = false
        Log.d("consumeErrorWrapper", "throwable: $it.localizedMessage")
    }
}

fun loadingStateWrapper(lvState: SingleLiveEvent<Boolean>): Consumer<Disposable> {
    return Consumer {
        lvState.value = true
    }
}

fun consumeErrorWrapperWithServerError(
    lvState: SingleLiveEvent<Boolean>,
    lvMessage: SingleLiveEvent<String>
): Consumer<Throwable> {
    return Consumer {
        lvState.value = false
        lvMessage.value = it.message
    }
}
