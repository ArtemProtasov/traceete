@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package org.letmecode.traceete.util

import java.text.SimpleDateFormat
import java.util.*

var yyyy_MM_HH_mm_ss = "yyyy-MM-dd HH:mm:ss"
var yyyy_dd_MM_HH_mm_ss = "yyyy/dd/MM HH:mm:ss"
var yyyy_dd_MM_HH = "yyyy-MM-dd"
var yyyy_dd_MM_HH_slash = "yyyy/MM/dd"

fun parseDate(input: String?, pattern: String): Date = try {
    SimpleDateFormat(pattern, Locale.getDefault()).parse(input)
} catch (e: Exception) {
    Date()
}

fun getDate(year: Int, month: Int, dayOfMonth: Int): Date {
    return GregorianCalendar(year, month, dayOfMonth).time
}

fun formatDate(locale: Date?, pattern: String): String? {
    return try {
        SimpleDateFormat(pattern, Locale.getDefault()).format(locale)
    } catch (e: java.lang.Exception) {
        null
    }
}