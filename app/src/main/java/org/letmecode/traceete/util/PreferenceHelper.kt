package org.letmecode.traceete.util

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class PreferenceHelper @Inject constructor(application: Context) {
    private var sharedPreferences: SharedPreferences? = null

    init {
        sharedPreferences = application.getSharedPreferences("main_setting", Context.MODE_PRIVATE)
    }

}