package org.letmecode.traceete.ui.common

import android.content.SharedPreferences
import javax.inject.Inject

class UserPreference @Inject constructor(sharedPreferences: SharedPreferences) {

    var sampleParam: String? by sharedPreferences.string(SAMPLE_PARAM, null)

    private companion object Key {
        const val SAMPLE_PARAM = "SAMPLE_PARAM"
    }

}