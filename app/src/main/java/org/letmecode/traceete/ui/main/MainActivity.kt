package org.letmecode.traceete.ui.main

import androidx.lifecycle.LifecycleOwner
import org.letmecode.traceete.R
import org.letmecode.traceete.ui.common.BaseActivity

class MainActivity : BaseActivity(), LifecycleOwner {

    override fun getLayoutResource(): Int = R.layout.activity_main

    override fun getNavigationResource(): Int = R.id.navHostFragment

}
