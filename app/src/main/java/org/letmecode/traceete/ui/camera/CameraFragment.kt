package org.letmecode.traceete.ui.camera

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.letmecode.traceete.R
import org.letmecode.traceete.databinding.FragmentCameraBinding
import org.letmecode.traceete.ui.common.BaseCleanFragment

class CameraFragment : BaseCleanFragment<CameraViewModel, FragmentCameraBinding>() {

    override fun getFragmentLayout(): Int = R.layout.fragment_camera

    override fun viewModelClass(): Class<CameraViewModel> = CameraViewModel::class.java

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

}