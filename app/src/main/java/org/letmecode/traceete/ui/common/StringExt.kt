package org.letmecode.traceete.ui.common

import android.util.Base64

fun encrypt(input: String): String {
    return try {
        Base64.encodeToString(input.toByteArray(), Base64.NO_WRAP)
    } catch (e: Exception) {
        input
    }
}

fun decrypt(input: String): String {
    return try {
        String(Base64.decode(input, Base64.NO_WRAP))
    } catch (e: Exception) {
        input
    }


}