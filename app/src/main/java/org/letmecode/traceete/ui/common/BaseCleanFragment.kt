package org.letmecode.traceete.ui.common

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import dagger.android.support.DaggerFragment
import org.letmecode.traceete.di.annotation.Injectable
import javax.inject.Inject

abstract class BaseCleanFragment<V : ViewModel, B : ViewDataBinding> : DaggerFragment(),
    Injectable {

    protected lateinit var navController: NavController
    protected lateinit var viewModel: V
    protected lateinit var binding: B
    protected lateinit var frContext: Context

    @Inject
    lateinit var userPreference: UserPreference

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    abstract fun getFragmentLayout(): Int
    abstract fun viewModelClass(): Class<V>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.frContext = context
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClass())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getFragmentLayout(), container, false)
        binding.lifecycleOwner = this
        navController = NavHostFragment.findNavController(this)
        return binding.root
    }

    fun navigate(destination: NavDirections) = with(findNavController()) {
        currentDestination?.getAction(destination.actionId)
            ?.let { navigate(destination) }
    }

    private fun findNavController(): NavController = NavHostFragment.findNavController(this)
}