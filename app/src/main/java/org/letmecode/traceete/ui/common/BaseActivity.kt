package org.letmecode.traceete.ui.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {

    protected var compositeDisposable = CompositeDisposable()

    protected abstract fun getLayoutResource(): Int

    protected abstract fun getNavigationResource(): Int

    protected var navController: NavController? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getLayoutResource() != 0)
            super.setContentView(getLayoutResource())
        AppCompatDelegate.setDefaultNightMode(
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
        )
        if (getNavigationResource() != 0) {
            navController = Navigation.findNavController(this, getNavigationResource())
        }
    }

    override fun onDestroy() {
        if (compositeDisposable.size() > 0)
            compositeDisposable.clear()
        compositeDisposable.dispose()
        super.onDestroy()
    }
}