package org.letmecode.traceete.ui.common

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import org.letmecode.traceete.R

inline fun <reified T : ViewModel> AppCompatActivity.injectViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProvider(this, factory)[T::class.java]
}

fun showToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}

fun showToast(context: Context, res: Int) {
    Toast.makeText(context, context.getString(res), Toast.LENGTH_LONG).show()
}

fun showSnack(activity: Activity?, res: Int) {
    activity?.let {
        val view = it.findViewById<View>(android.R.id.content)
        Snackbar.make(view, it.getString(res), Snackbar.LENGTH_SHORT).show()
    }
}

fun showSnack(activity: Activity?, message: String?) {
    if (activity != null && message != null) {
        val view = activity.findViewById<View>(android.R.id.content)
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }
}

fun navigateClearTasks(activity: Activity?, destination: Class<*>) {
    val intent = Intent(activity, destination)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    activity?.startActivity(intent)
}


fun navigate(activity: Activity?, destination: Class<*>) {
    val intent = Intent(activity, destination)
    activity?.startActivity(intent)
    activity?.finish()
}

fun navigate(activity: Activity?, destination: Class<*>, arguments: MutableMap<String, *>) {
    val intent = Intent(activity, destination)
    putArguments(intent, arguments)
    activity?.startActivity(intent)
    activity?.finish()
}

fun putArguments(intent: Intent, arguments: MutableMap<String, *>) {
    for ((key, value) in arguments) {
        when (value) {
            String -> intent.putExtra(key, value as String)
            Int -> intent.putExtra(key, value as Int)
            Boolean -> intent.putExtra(key, value as Boolean)
            Float -> intent.putExtra(key, value as Float)
            Double -> intent.putExtra(key, value as Double)
        }
    }
}

fun navigate(activity: Activity?, action: String, sUri: String) {
    val intent = Intent(action)
    intent.data = Uri.parse(sUri)
    activity?.startActivity(intent)
}

fun navigate(context: Context?, action: String, sUri: String?) {
    val intent = Intent(action)
    intent.data = Uri.parse(sUri)
    context?.startActivity(intent)
}
