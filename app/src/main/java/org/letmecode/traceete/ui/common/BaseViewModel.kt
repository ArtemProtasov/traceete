package org.letmecode.traceete.ui.common

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import org.letmecode.traceete.util.SingleLiveEvent

abstract class BaseViewModel : ViewModel() {

    protected val _lvProgress = SingleLiveEvent<Boolean>()
    protected val _lvMessage = SingleLiveEvent<String>()

    val lvMessage: SingleLiveEvent<String>
        get() = _lvMessage

    protected var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}