package org.letmecode.traceete.retrofit

data class ApiError(val statusCode: Int = 0,
                    val message: String? = null)