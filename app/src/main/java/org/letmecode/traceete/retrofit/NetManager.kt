package org.letmecode.traceete.retrofit

import android.content.Context
import android.net.ConnectivityManager
import org.letmecode.traceete.R

class NetManager  ( private var applicationContext: Context) {
    private var status: Boolean = false

    val isConnectedToInternet: Boolean
        get() {
            val conManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val ni = conManager.activeNetworkInfo
            return ni != null && ni.isConnected
        }

    val message: String
        get() {
            return applicationContext.getString(R.string.common_no_internet_connection)
        }
}