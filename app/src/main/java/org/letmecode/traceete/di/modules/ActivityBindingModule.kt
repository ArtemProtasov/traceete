package org.letmecode.traceete.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.letmecode.traceete.ui.main.MainActivity
import org.letmecode.traceete.di.annotation.ActivityScoped

@Module
abstract class ActivityBindingModule{

    @ActivityScoped
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun provideMainActivity(): MainActivity

}