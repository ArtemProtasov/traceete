package org.letmecode.traceete.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.letmecode.traceete.ui.camera.CameraFragment

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeCameraFragment(): CameraFragment

}