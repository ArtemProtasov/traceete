package org.letmecode.traceete.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import org.letmecode.traceete.ui.common.UserPreference
import org.letmecode.traceete.util.PreferenceHelper
import javax.inject.Singleton

@Module
class HelpersModule {

    @Singleton
    @Provides
    fun providePreferenceHelper(application: Application): PreferenceHelper {
        return PreferenceHelper(application)
    }

    @Singleton
    @Provides
    fun provideUserPreference(application: Application): UserPreference {
        return UserPreference(
            application.getSharedPreferences(
                "user_setting",
                Context.MODE_PRIVATE
            )
        )
    }


}