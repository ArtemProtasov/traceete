package org.letmecode.traceete.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import org.letmecode.traceete.TraceeteApplication
import org.letmecode.traceete.di.modules.*
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        FragmentModule::class,
        ViewModelModule::class,
        NetworkClientModule::class,
        NetworkApiModule::class,
        RepositoryModule::class,
        HelpersModule::class
    ]
)
interface AppComponent : AndroidInjector<TraceeteApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

}